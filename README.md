# tafelberg

## API Endpoints for Availability

The base url for the API server is

```
https://us-central1-tafelberg-1da0a.cloudfunctions.net/properties
```

The api endpoints to return a list a available dates for the next 365 days are

```
/simonstown
/muizenberg
/seapoint
/pierheaven
```

The endpoint `/` returns a list of all properties and their endpoints